# README #

This is Glenn's Gases. It adds gases among other things to Minecraft using the [Gases Framework](https://bitbucket.org/jamieswhiteshirt/gases-framework).

## To be able to run and develop Glenn's Gases in your workspace, you must follow these steps ##

**For Eclipse users:**

* Download a Minecraft Forge distribution and copy its eclipse folder into the workspace.
* Run *gradlew setupDevWorkspace* or *gradlew setupDecompWorkspace* in your workspace.
* Run *gradlew eclipse* in your workspace.
* Open the workspace with Eclipse.
* Open your run configurations and add the following VM argument to both Client and Server: *-Dfml.coreMods.load=glenn.gases.common.core.GGFMLLoadingPlugin*
* Install Gases Framework in your workspace.

**For IntelliJ IDEA users:**

* Run *gradlew setupDevWorkspace* or *gradlew setupDecompWorkspace* in your workspace.
* Run *gradlew idea* in your workspace.
* Open the generated project file in IntelliJ IDEA.
* Open Run/Debug Configurations.
* Add the following argument to both Client and Server application VM options: *-Dfml.coreMods.load=glenn.gases.common.core.GGFMLLoadingPlugin*
* Install Gases Framework in your workspace.

## Installing Gases Framework ##

**In the development workspace:** When installing the Gases Framework in a workspace, a development build must be installed. Development builds are not distributed and must be [built manually](https://bitbucket.org/jamieswhiteshirt/gases-framework). Place the development build in the /eclipse/mods folder of your workspace.

**In Minecraft:** Download a Gases Framework distribution from the [download page](http://jamieswhiteshirt.com/minecraft/mods/gases/download/). Place the downloaded file in 
/.minecraft/mods.
