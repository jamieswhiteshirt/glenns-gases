package glenn.gases.client.render;

import glenn.gases.common.entity.EntityFX;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderFX extends Render
{
	public RenderFX()
	{

	}

	private void renderFX(EntityFX entity, double x, double y, double z, float partialTick)
	{
		this.bindEntityTexture(entity);

		float red = (float) (entity.getColor() >> 24 & 255) / 255.0F;
		float green = (float) (entity.getColor() >> 16 & 255) / 255.0F;
		float blue = (float) (entity.getColor() >> 8 & 255) / 255.0F;
		float alpha = (float) (entity.getColor() & 255) / 255.0F;
		GL11.glColor4f(red, green, blue, alpha);

		entity.render(Tessellator.instance, renderManager, partialTick, x, y, z);
	}

	@Override
	public void doRender(Entity entity, double d0, double d1, double d2, float f, float f1)
	{
		renderFX((EntityFX) entity, d0, d1, d2, f1);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return ((EntityFX) entity).getTexture();
	}
}