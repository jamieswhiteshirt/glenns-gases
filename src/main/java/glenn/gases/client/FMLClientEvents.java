package glenn.gases.client;

import glenn.gases.Gases;
import glenn.gases.common.item.ItemRefinedDiabaline;
import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.world.World;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;

/** Client event handler. Registered in ClientProxy. */
public class FMLClientEvents
{
	/** Handles setting refined diabaline 'glow state'. */
	@SubscribeEvent
	public void onClientTick(ClientTickEvent event)
	{
		EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
		if (player == null)
			return;

		World world = player.worldObj;
		if (world == null)
			return;

		((ItemRefinedDiabaline) Gases.items.refinedDiabaline).glowState = isSurroundedByGas(world, (int) player.posX, (int) player.posY, (int) player.posZ, 3);
	}

	private boolean isSurroundedByGas(World world, int x, int y, int z, int radius)
	{
		for (int i = -radius; i <= radius; i++)
		{
			for (int j = -radius; j <= radius; j++)
			{
				for (int k = -radius; k <= radius; k++)
				{
					GasType type = GFAPI.implementation.getGasType(world, x + i, y + j, z + k);
					if (type != null && type.isIndustrial && type != GFAPI.gasTypeAir)
					{
						return true;
					}
				}
			}
		}

		return false;
	}
}
