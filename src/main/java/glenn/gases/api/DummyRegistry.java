package glenn.gases.api;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class DummyRegistry implements IGGRegistry
{
	@Override
	public void registerCoalDustEmissionBlock(Block block)
	{
	}

	@Override
	public boolean isCoalDustEmissionBlock(Block block)
	{
		return false;
	}

	@Override
	public void registerRustedBlock(Block block, Block rustedBlock)
	{
	}

	@Override
	public Block getRustedBlock(Block block)
	{
		return null;
	}

	@Override
	public Block getUnrustedBlock(Block rustedBlock)
	{
		return null;
	}

	@Override
	public void registerRustedItem(Item item, Item rustedItem)
	{
	}

	@Override
	public Item getRustedItem(Item item)
	{
		return null;
	}

	@Override
	public Item getUnrustedItem(Item rustedItem)
	{
		return null;
	}

	@Override
	public void registerRustableMaterial(String material)
	{
	}

	@Override
	public boolean isRustableMaterial(String material)
	{
		return false;
	}
}
