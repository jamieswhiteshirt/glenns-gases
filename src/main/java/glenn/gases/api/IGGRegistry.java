package glenn.gases.api;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

/**
 * An interface to connect Glenn's Gases to the registry of Glenn's Gases.
 * 
 * @author Erlend
 */
public interface IGGRegistry
{
	/**
	 * Register a coal dust emission block, e.g. a block that will emit coal
	 * dust when broken.
	 * 
	 * @param block
	 *            The coal dust emission block
	 */
	void registerCoalDustEmissionBlock(Block block);

	/**
	 * Returns true if the block is a coal dust emission block, e.g. a block
	 * that will emit coal dust when broken.
	 * 
	 * @param block
	 *            The block in question
	 * @return True if the block is a coal dust emission block
	 */
	boolean isCoalDustEmissionBlock(Block block);

	/**
	 * Register a block with a rusted counterpart.
	 * 
	 * @param block
	 *            The block
	 * @param rustedBlock
	 *            The rusted block
	 */
	void registerRustedBlock(Block block, Block rustedBlock);

	/**
	 * Get the rusted block for this block.
	 * 
	 * @param block
	 *            The block
	 * @return The rusted block, or null if none is assigned
	 */
	Block getRustedBlock(Block block);

	/**
	 * Get the unrusted block for this rusted block.
	 * 
	 * @param rustedBlock
	 *            The block
	 * @return The unrusted block, or null if none is assigned
	 */
	Block getUnrustedBlock(Block rustedBlock);

	/**
	 * Register an item with a rusted counterpart.
	 * 
	 * @param item
	 *            The item
	 * @param rustedItem
	 *            The rusted item
	 */
	void registerRustedItem(Item item, Item rustedItem);

	/**
	 * Get the rusted item for this item. If the item is a block, its rusted
	 * counterpart is returned.
	 * 
	 * @param item
	 *            The item
	 * @return The rusted item or block, or null if none is assigned
	 */
	Item getRustedItem(Item item);

	/**
	 * Get the unrusted item for this item. If the item is a block, its unrusted
	 * counterpart is returned.
	 * 
	 * @param rustedItem
	 *            The rusted item
	 * @return The unrusted item or block, or null if none is assigned
	 */
	Item getUnrustedItem(Item rustedItem);

	/**
	 * Register a tool/armor material that can be damaged by rust.
	 * 
	 * @param material
	 *            The name of the material
	 */
	void registerRustableMaterial(String material);

	/**
	 * Can the material be damaged by rust?
	 * 
	 * @param material
	 *            The name of the material
	 * @return True if the material can be damaged by rust
	 */
	boolean isRustableMaterial(String material);
}
