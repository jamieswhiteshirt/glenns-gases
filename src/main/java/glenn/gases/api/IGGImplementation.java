package glenn.gases.api;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.Random;

/**
 * An interface to connect the GGAPI to the functionality of Glenn's Gases.
 * 
 * @author Erlend
 */
public interface IGGImplementation
{
	/**
	 * Try to rust the item. Returns the new rusted/damaged item, or null if it
	 * completely broken.
	 * 
	 * @param itemstack
	 *            The itemstack to rust
	 * @param random
	 *            The random object
	 * @return The new itemstack, null if broken
	 */
	ItemStack tryRustItem(ItemStack itemstack, Random random);

	/**
	 * Can rust damage this item?
	 * 
	 * @param item
	 *            The item to rust
	 * @return True if the item can be damaged by rust
	 */
	boolean canRustDamageItem(Item item);
}
