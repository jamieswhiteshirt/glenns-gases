package glenn.gases.common;

import glenn.gases.Gases;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTab extends CreativeTabs
{
	public CreativeTab(String label)
	{
		super(label);
	}

	@Override
	public Item getTabIconItem()
	{
		return Gases.items.refinedDiabaline;
	}
}
