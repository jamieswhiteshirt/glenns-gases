package glenn.gases.common.core;

import glenn.gases.Gases;

public class Helper
{
	public static int getSteamReactionAmount()
	{
		return Gases.configurations.gases.steam.amountOnReaction;
	}

	public static int getFireSmokeAmount()
	{
		return Gases.configurations.gases.smoke.fireSmokeAmount;
	}
}
