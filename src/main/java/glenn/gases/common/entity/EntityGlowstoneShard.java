package glenn.gases.common.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import glenn.gases.Gases;

import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.EnchantmentThorns;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IProjectile;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S2BPacketChangeGameState;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityGlowstoneShard extends Entity implements IProjectile
{
	private int xTile = -1;
	private int yTile = -1;
	private int zTile = -1;
	private Block inTile;
	private int inData;
	private boolean inGround;

	/** Seems to be some sort of timer for animating an arrow. */
	public int arrowShake;

	/** The owner of this arrow. */
	public Entity shootingEntity;
	private int ticksInGround;
	private int ticksInAir;
	private double damage = 2.0D;

	/** The amount of knockback an arrow applies when it hits a mob. */
	private int knockbackStrength;

	public EntityGlowstoneShard(World par1World)
	{
		super(par1World);
		this.renderDistanceWeight = 10.0D;
		this.setSize(0.5F, 0.5F);
	}

	public EntityGlowstoneShard(World par1World, double par2, double par4, double par6)
	{
		super(par1World);
		this.renderDistanceWeight = 10.0D;
		this.setSize(0.5F, 0.5F);
		this.setPosition(par2, par4, par6);
		this.yOffset = 0.0F;
	}

	public EntityGlowstoneShard(World par1World, EntityLivingBase par2EntityLivingBase)
	{
		super(par1World);
		this.setSize(0.25F, 0.25F);
		this.setLocationAndAngles(par2EntityLivingBase.posX, par2EntityLivingBase.posY + (double) par2EntityLivingBase.getEyeHeight(), par2EntityLivingBase.posZ, par2EntityLivingBase.rotationYaw, par2EntityLivingBase.rotationPitch);
		this.posX -= (double) (MathHelper.cos(this.rotationYaw / 180.0F * (float) Math.PI) * 0.16F);
		this.posY -= 0.10000000149011612D;
		this.posZ -= (double) (MathHelper.sin(this.rotationYaw / 180.0F * (float) Math.PI) * 0.16F);
		this.setPosition(this.posX, this.posY, this.posZ);
		this.yOffset = 0.0F;
		float f = 0.4F;
		this.motionX = (double) (-MathHelper.sin(this.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float) Math.PI) * f);
		this.motionZ = (double) (MathHelper.cos(this.rotationYaw / 180.0F * (float) Math.PI) * MathHelper.cos(this.rotationPitch / 180.0F * (float) Math.PI) * f);
		this.motionY = (double) (-MathHelper.sin((this.rotationPitch) / 180.0F * (float) Math.PI) * f);
		this.setThrowableHeading(this.motionX, this.motionY, this.motionZ, 3.0F, 1.0F);
	}

	@Override
	protected void entityInit()
	{
		this.dataWatcher.addObject(16, Byte.valueOf((byte) 0));
	}

	/**
	 * Similar to setArrowHeading, it's point the throwable entity to a x, y, z
	 * direction.
	 */
	@Override
	public void setThrowableHeading(double par1, double par3, double par5, float par7, float par8)
	{
		float f2 = MathHelper.sqrt_double(par1 * par1 + par3 * par3 + par5 * par5);
		par1 /= (double) f2;
		par3 /= (double) f2;
		par5 /= (double) f2;
		par1 += this.rand.nextGaussian() * (double) (this.rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double) par8;
		par3 += this.rand.nextGaussian() * (double) (this.rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double) par8;
		par5 += this.rand.nextGaussian() * (double) (this.rand.nextBoolean() ? -1 : 1) * 0.007499999832361937D * (double) par8;
		par1 *= (double) par7;
		par3 *= (double) par7;
		par5 *= (double) par7;
		this.motionX = par1;
		this.motionY = par3;
		this.motionZ = par5;
		float f3 = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
		this.prevRotationYaw = this.rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
		this.prevRotationPitch = this.rotationPitch = (float) (Math.atan2(par3, (double) f3) * 180.0D / Math.PI);
		this.ticksInGround = 0;
	}

	@SideOnly(Side.CLIENT)

	/**
	 * Sets the position and rotation. Only difference from the other one is no
	 * bounding on the rotation. Args: posX, posY, posZ, yaw, pitch
	 */
	@Override
	public void setPositionAndRotation2(double par1, double par3, double par5, float par7, float par8, int par9)
	{
		this.setPosition(par1, par3, par5);
		this.setRotation(par7, par8);
	}

	/**
	 * Sets the velocity to the args. Args: x, y, z
	 */
	@Override
	public void setVelocity(double par1, double par3, double par5)
	{
		this.motionX = par1;
		this.motionY = par3;
		this.motionZ = par5;

		if (this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F)
		{
			float f = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
			this.prevRotationYaw = this.rotationYaw = (float) (Math.atan2(par1, par5) * 180.0D / Math.PI);
			this.prevRotationPitch = this.rotationPitch = (float) (Math.atan2(par3, (double) f) * 180.0D / Math.PI);
			this.prevRotationPitch = this.rotationPitch;
			this.prevRotationYaw = this.rotationYaw;
			this.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, this.rotationPitch);
			this.ticksInGround = 0;
		}
	}

	/**
	 * Called to update the entity's position/logic.
	 */
	@Override
	public void onUpdate()
	{
		super.onUpdate();

		if (this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F)
		{
			float f1 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
			this.prevRotationYaw = (this.rotationYaw = (float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / 3.141592741012573D));
			this.prevRotationPitch = (this.rotationPitch = (float) (Math.atan2(this.motionY, f1) * 180.0D / 3.141592741012573D));
		}

		Block localBlock = this.worldObj.getBlock(this.xTile, this.yTile, this.zTile);
		if (localBlock.getMaterial() != Material.air)
		{
			localBlock.setBlockBoundsBasedOnState(this.worldObj, this.xTile, this.yTile, this.zTile);
			AxisAlignedBB localAxisAlignedBB = localBlock.getCollisionBoundingBoxFromPool(this.worldObj, this.xTile, this.yTile, this.zTile);
			// if(localAxisAlignedBB != null &&
			// localAxisAlignedBB.isVecInside(this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX,
			// this.posY, this.posZ)))
			if (localAxisAlignedBB != null && localAxisAlignedBB.isVecInside(Vec3.createVectorHelper(this.posX, this.posY, this.posZ)))
			{
				this.inGround = true;
			}
		}

		if (this.arrowShake > 0)
			this.arrowShake -= 1;

		if (this.inGround)
		{
			int i = this.worldObj.getBlockMetadata(this.xTile, this.yTile, this.zTile);
			if (localBlock != this.inTile || i != this.inData)
			{
				this.inGround = false;

				this.motionX *= this.rand.nextFloat() * 0.2F;
				this.motionY *= this.rand.nextFloat() * 0.2F;
				this.motionZ *= this.rand.nextFloat() * 0.2F;
				this.ticksInGround = 0;
				this.ticksInAir = 0;
				return;
			}
			this.ticksInGround += 1;
			if (this.ticksInGround == 1200)
			{
				setDead();
			}
			return;
		}

		this.ticksInAir += 1;

		// Vec3 localVec31 =
		// this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX, this.posY,
		// this.posZ);
		// Vec3 localVec32 =
		// this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX +
		// this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
		Vec3 localVec31 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
		Vec3 localVec32 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
		MovingObjectPosition localMovingObjectPosition1 = this.worldObj.func_147447_a(localVec31, localVec32, false, true, false);

		// localVec31 =
		// this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX, this.posY,
		// this.posZ);
		// localVec32 =
		// this.worldObj.getWorldVec3Pool().getVecFromPool(this.posX +
		// this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
		localVec31 = Vec3.createVectorHelper(this.posX, this.posY, this.posZ);
		localVec32 = Vec3.createVectorHelper(this.posX + this.motionX, this.posY + this.motionY, this.posZ + this.motionZ);
		if (localMovingObjectPosition1 != null)
		{
			// localVec32 =
			// this.worldObj.getWorldVec3Pool().getVecFromPool(localMovingObjectPosition1.hitVec.xCoord,
			// localMovingObjectPosition1.hitVec.yCoord,
			// localMovingObjectPosition1.hitVec.zCoord);
			localVec32 = Vec3.createVectorHelper(localMovingObjectPosition1.hitVec.xCoord, localMovingObjectPosition1.hitVec.yCoord, localMovingObjectPosition1.hitVec.zCoord);
		}
		Entity localObject1 = null;
		List localList = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.boundingBox.addCoord(this.motionX, this.motionY, this.motionZ).expand(1.0D, 1.0D, 1.0D));
		double d1 = 0.0D;
		Object localObject2;
		for (int j = 0; j < localList.size(); j++)
		{
			Entity localEntity = (Entity) localList.get(j);
			if ((localEntity.canBeCollidedWith()) && ((localEntity != this.shootingEntity) || (this.ticksInAir >= 5)))
			{
				float f5 = 0.3F;
				localObject2 = localEntity.boundingBox.expand(f5, f5, f5);
				MovingObjectPosition localMovingObjectPosition2 = ((AxisAlignedBB) localObject2).calculateIntercept(localVec31, localVec32);
				if (localMovingObjectPosition2 != null)
				{
					double d2 = localVec31.distanceTo(localMovingObjectPosition2.hitVec);
					if ((d2 < d1) || (d1 == 0.0D))
					{
						localObject1 = localEntity;
						d1 = d2;
					}
				}
			}
		}

		if (localObject1 != null)
		{
			localMovingObjectPosition1 = new MovingObjectPosition(localObject1);
		}

		if (localMovingObjectPosition1 != null && localMovingObjectPosition1.entityHit != null && localMovingObjectPosition1.entityHit instanceof EntityPlayer)
		{
			EntityPlayer localEntityPlayer = (EntityPlayer) localMovingObjectPosition1.entityHit;
			if (localEntityPlayer.capabilities.disableDamage || (this.shootingEntity instanceof EntityPlayer && !((EntityPlayer) this.shootingEntity).canAttackPlayer(localEntityPlayer)))
			{
				localMovingObjectPosition1 = null;
			}
		}
		float f7;
		if (localMovingObjectPosition1 != null)
		{
			float f2;
			if (localMovingObjectPosition1.entityHit != null)
			{
				f2 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
				int m = MathHelper.ceiling_double_int(f2 * this.damage);

				if (getIsCritical())
					m += this.rand.nextInt(m / 2 + 2);

				DamageSource localDamageSource = null;
				if (this.shootingEntity == null)
				{
					localDamageSource = (new EntityDamageSourceIndirect("gg_glowstoneShard", this, this)).setProjectile();
				}
				else
				{
					localDamageSource = (new EntityDamageSourceIndirect("gg_glowstoneShard", this, shootingEntity)).setProjectile();
				}

				if (isBurning() && !(localMovingObjectPosition1.entityHit instanceof EntityEnderman))
				{
					localMovingObjectPosition1.entityHit.setFire(5);
				}
				if (localMovingObjectPosition1.entityHit.attackEntityFrom(localDamageSource, m))
				{
					if (localMovingObjectPosition1.entityHit instanceof EntityLivingBase)
					{
						localObject2 = (EntityLivingBase) localMovingObjectPosition1.entityHit;

						if (this.knockbackStrength > 0)
						{
							f7 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
							if (f7 > 0.0F)
							{
								localMovingObjectPosition1.entityHit.addVelocity(this.motionX * this.knockbackStrength * 0.6000000238418579D / f7, 0.1D, this.motionZ * this.knockbackStrength * 0.6000000238418579D / f7);
							}
						}

						if (this.shootingEntity != null && this.shootingEntity instanceof EntityLivingBase)
						{
							// EnchantmentHelper.func_151384_a((EntityLivingBase)localObject2,
							// this.shootingEntity);
							// EnchantmentHelper.func_151385_b((EntityLivingBase)this.shootingEntity,
							// (Entity)localObject2);
						}

						if (this.shootingEntity != null && localMovingObjectPosition1.entityHit != this.shootingEntity && localMovingObjectPosition1.entityHit instanceof EntityPlayer && this.shootingEntity instanceof EntityPlayerMP)
						{
							((EntityPlayerMP) this.shootingEntity).playerNetServerHandler.sendPacket(new S2BPacketChangeGameState(6, 0.0F));
						}
					}
					playSound("random.bowhit", 1.0F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
					if (!(localMovingObjectPosition1.entityHit instanceof EntityEnderman))
						setDead();
				}
				else
				{
					this.motionX *= -0.1000000014901161D;
					this.motionY *= -0.1000000014901161D;
					this.motionZ *= -0.1000000014901161D;
					this.rotationYaw += 180.0F;
					this.prevRotationYaw += 180.0F;
					this.ticksInAir = 0;
				}
			}
			else
			{
				this.xTile = localMovingObjectPosition1.blockX;
				this.yTile = localMovingObjectPosition1.blockY;
				this.zTile = localMovingObjectPosition1.blockZ;
				this.inTile = localBlock;
				this.inData = this.worldObj.getBlockMetadata(this.xTile, this.yTile, this.zTile);
				this.motionX = ((float) (localMovingObjectPosition1.hitVec.xCoord - this.posX));
				this.motionY = ((float) (localMovingObjectPosition1.hitVec.yCoord - this.posY));
				this.motionZ = ((float) (localMovingObjectPosition1.hitVec.zCoord - this.posZ));
				f2 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionY * this.motionY + this.motionZ * this.motionZ);
				this.posX -= this.motionX / f2 * 0.0500000007450581D;
				this.posY -= this.motionY / f2 * 0.0500000007450581D;
				this.posZ -= this.motionZ / f2 * 0.0500000007450581D;

				playSound("dig.glass", 0.7F, 1.2F / (this.rand.nextFloat() * 0.2F + 0.9F));
				this.inGround = true;
				this.arrowShake = 7;
				setIsCritical(false);

				if (this.inTile.getMaterial() != Material.air)
				{
					this.inTile.onEntityCollidedWithBlock(this.worldObj, this.xTile, this.yTile, this.zTile, this);

					if (!worldObj.isRemote && rand.nextInt(5) != 0)
					{
						worldObj.removeEntity(this);
					}
				}
			}
		}

		if (getIsCritical())
		{
			for (int k = 0; k < 4; k++)
			{
				this.worldObj.spawnParticle("crit", this.posX + this.motionX * k / 4.0D, this.posY + this.motionY * k / 4.0D, this.posZ + this.motionZ * k / 4.0D, -this.motionX, -this.motionY + 0.2D, -this.motionZ);
			}
		}

		this.posX += this.motionX;
		this.posY += this.motionY;
		this.posZ += this.motionZ;

		float f3 = MathHelper.sqrt_double(this.motionX * this.motionX + this.motionZ * this.motionZ);
		this.rotationYaw = ((float) (Math.atan2(this.motionX, this.motionZ) * 180.0D / 3.141592741012573D));
		this.rotationPitch = ((float) (Math.atan2(this.motionY, f3) * 180.0D / 3.141592741012573D));

		while (this.rotationPitch - this.prevRotationPitch < -180.0F)
			this.prevRotationPitch -= 360.0F;
		while (this.rotationPitch - this.prevRotationPitch >= 180.0F)
			this.prevRotationPitch += 360.0F;
		while (this.rotationYaw - this.prevRotationYaw < -180.0F)
			this.prevRotationYaw -= 360.0F;
		while (this.rotationYaw - this.prevRotationYaw >= 180.0F)
			this.prevRotationYaw += 360.0F;

		this.rotationPitch = (this.prevRotationPitch + (this.rotationPitch - this.prevRotationPitch) * 0.2F);
		this.rotationYaw = (this.prevRotationYaw + (this.rotationYaw - this.prevRotationYaw) * 0.2F);

		float f4 = 0.99F;
		float f6 = 0.05F;

		if (isInWater())
		{
			for (int n = 0; n < 4; n++)
			{
				f7 = 0.25F;
				this.worldObj.spawnParticle("bubble", this.posX - this.motionX * f7, this.posY - this.motionY * f7, this.posZ - this.motionZ * f7, this.motionX, this.motionY, this.motionZ);
			}
			f4 = 0.8F;
		}

		if (isWet())
			extinguish();

		this.motionX *= f4;
		this.motionY *= f4;
		this.motionZ *= f4;
		this.motionY -= f6;

		setPosition(this.posX, this.posY, this.posZ);

		func_145775_I();
	}

	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
	{
		par1NBTTagCompound.setShort("xTile", (short) this.xTile);
		par1NBTTagCompound.setShort("yTile", (short) this.yTile);
		par1NBTTagCompound.setShort("zTile", (short) this.zTile);
		par1NBTTagCompound.setByte("inTile", (byte) Block.getIdFromBlock(this.inTile));
		par1NBTTagCompound.setByte("inData", (byte) this.inData);
		par1NBTTagCompound.setByte("shake", (byte) this.arrowShake);
		par1NBTTagCompound.setByte("inGround", (byte) (this.inGround ? 1 : 0));
		par1NBTTagCompound.setDouble("damage", this.damage);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
	{
		this.xTile = par1NBTTagCompound.getShort("xTile");
		this.yTile = par1NBTTagCompound.getShort("yTile");
		this.zTile = par1NBTTagCompound.getShort("zTile");
		this.inTile = Block.getBlockById(par1NBTTagCompound.getByte("inTile") & 255);
		this.inData = par1NBTTagCompound.getByte("inData") & 255;
		this.arrowShake = par1NBTTagCompound.getByte("shake") & 255;
		this.inGround = par1NBTTagCompound.getByte("inGround") == 1;

		if (par1NBTTagCompound.hasKey("damage"))
		{
			this.damage = par1NBTTagCompound.getDouble("damage");
		}
	}

	/**
	 * Called by a player entity when they collide with an entity
	 */
	@Override
	public void onCollideWithPlayer(EntityPlayer par1EntityPlayer)
	{
		if (!this.worldObj.isRemote && this.inGround && this.arrowShake <= 0)
		{
			if (par1EntityPlayer.capabilities.isCreativeMode)
			{
				this.playSound("random.pop", 0.2F, ((this.rand.nextFloat() - this.rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
				if (par1EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Gases.items.glowstoneShard, 1)))
				{
					par1EntityPlayer.onItemPickup(this, 1);
					this.setDead();
				}
			}
		}
	}

	/**
	 * returns if this entity triggers Block.onEntityWalking on the blocks they
	 * walk on. used for spiders and wolves to prevent them from trampling crops
	 */
	@Override
	protected boolean canTriggerWalking()
	{
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public float getShadowSize()
	{
		return 0.0F;
	}

	public void setDamage(double par1)
	{
		this.damage = par1;
	}

	public double getDamage()
	{
		return this.damage;
	}

	/**
	 * Sets the amount of knockback the arrow applies when it hits a mob.
	 */
	public void setKnockbackStrength(int par1)
	{
		this.knockbackStrength = par1;
	}

	/**
	 * If returns false, the item will not inflict any damage against entities.
	 */
	@Override
	public boolean canAttackWithItem()
	{
		return false;
	}

	/**
	 * Whether the arrow has a stream of critical hit particles flying behind
	 * it.
	 */
	public void setIsCritical(boolean par1)
	{
		byte b0 = this.dataWatcher.getWatchableObjectByte(16);

		if (par1)
		{
			this.dataWatcher.updateObject(16, Byte.valueOf((byte) (b0 | 1)));
		}
		else
		{
			this.dataWatcher.updateObject(16, Byte.valueOf((byte) (b0 & -2)));
		}
	}

	/**
	 * Whether the arrow has a stream of critical hit particles flying behind
	 * it.
	 */
	public boolean getIsCritical()
	{
		byte b0 = this.dataWatcher.getWatchableObjectByte(16);
		return (b0 & 1) != 0;
	}
}
