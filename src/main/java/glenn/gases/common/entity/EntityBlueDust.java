package glenn.gases.common.entity;

import glenn.gases.Gases;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityBlueDust extends EntityDust
{
	public EntityBlueDust(World world)
	{
		super(world);
	}

	public EntityBlueDust(World world, double x, double y, double z)
	{
		super(world, x, y, z);
	}

	public EntityBlueDust(World world, EntityLivingBase thrower)
	{
		super(world, thrower);
	}

	@Override
	public int getColor()
	{
		return Gases.gasTypeCorrosive.color | 0xFF;
	}
}