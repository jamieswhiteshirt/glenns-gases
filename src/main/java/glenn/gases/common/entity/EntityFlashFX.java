package glenn.gases.common.entity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import glenn.moddingutils.DVec;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityFlashFX extends EntityFX
{
	private static final ResourceLocation texture = new ResourceLocation("gases:textures/effects/flash.png");

	public EntityFlashFX(World world)
	{
		super(world);
	}

	public EntityFlashFX(World world, double x, double y, double z)
	{
		super(world, x, y, z);

		maxLifetime = 4;

		for (int i = 0; i < 10; i++)
		{
			double d = worldObj.rand.nextDouble();
			DVec v = DVec.randomNormalizedVec(worldObj.rand).scale(1.0D - d * d);
			worldObj.spawnEntityInWorld(new EntityFlashSparkFX(worldObj, posX, posY, posZ, v.x, v.y, v.z));
		}
	}

	@Override
	public double getScale(float partialTick)
	{
		double d = 5.0D * (4 - lifetime - partialTick);
		return d > 0.0D ? d : 0.0D;
	}

	@Override
	public ResourceLocation getTexture()
	{
		return texture;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getBrightnessForRender(float par1)
	{
		return 0xF000F0;
	}

	@Override
	public void render(Tessellator tessellator, RenderManager renderManager, float partialTick, double x, double y, double z)
	{
		// Vec3 vec1 = worldObj.getWorldVec3Pool().getVecFromPool(posX, posY,
		// posZ);
		// Vec3 vec2 =
		// worldObj.getWorldVec3Pool().getVecFromPool(renderManager.viewerPosX,
		// renderManager.viewerPosY, renderManager.viewerPosZ);
		Vec3 vec1 = Vec3.createVectorHelper(posX, posY, posZ);
		Vec3 vec2 = Vec3.createVectorHelper(renderManager.viewerPosX, renderManager.viewerPosY, renderManager.viewerPosZ);
		MovingObjectPosition movingObjectPosition = this.worldObj.func_147447_a(vec1, vec2, false, true, false);

		if (movingObjectPosition == null)
		{
			GL11.glDepthMask(true);
			GL11.glDepthFunc(GL11.GL_ALWAYS);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_DEPTH_TEST);
			super.render(tessellator, renderManager, partialTick, x, y, z);
			GL11.glDepthFunc(GL11.GL_LEQUAL);
			GL11.glDepthMask(false);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
		}
	}
}
