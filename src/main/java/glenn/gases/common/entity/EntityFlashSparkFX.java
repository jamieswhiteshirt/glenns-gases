package glenn.gases.common.entity;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class EntityFlashSparkFX extends EntityFX
{
	private static final ResourceLocation texture = new ResourceLocation("gases:textures/effects/flash_spark.png");

	private double[] prevMotionX = new double[5];
	private double[] prevMotionY = new double[5];
	private double[] prevMotionZ = new double[5];

	public EntityFlashSparkFX(World world)
	{
		super(world);
	}

	public EntityFlashSparkFX(World world, double x, double y, double z, double xv, double yv, double zv)
	{
		super(world, x, y, z);
		this.motionX = xv;
		this.motionY = yv;
		this.motionZ = zv;

		maxLifetime = 20 + world.rand.nextInt(10);
	}

	@Override
	public double getScale(float partialTick)
	{
		return 0.05D;
	}

	@Override
	public ResourceLocation getTexture()
	{
		return texture;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getBrightnessForRender(float par1)
	{
		return 0xF000F0;
	}

	@Override
	public void onUpdate()
	{
		if (motionY == 0.0D)
		{
			motionY = -prevMotionY[0] * 0.75D;
			motionX *= 0.9D;
			motionZ *= 0.9D;
		}
		else
		{
			motionY -= 0.1D;
		}

		push(motionX, prevMotionX);
		push(motionY, prevMotionY);
		push(motionZ, prevMotionZ);

		super.onUpdate();
	}

	public void render(Tessellator tessellator, RenderManager renderManager, float partialTick, double x, double y, double z)
	{
		x = this.posX - renderManager.viewerPosX;
		y = this.posY - renderManager.viewerPosY;
		z = this.posZ - renderManager.viewerPosZ;

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);

		tessellator.startDrawing(GL11.GL_LINE_STRIP);
		tessellator.addVertexWithUV(x, y, z, 0.0D, 0.0D);
		for (int i = 0; i < 5; i++)
		{
			x -= prevMotionX[i];
			y -= prevMotionY[i];
			z -= prevMotionZ[i];
			// tessellator.setColorRGBA_F(1.0F, 1.0F, 1.0F, (4 - i) / 5.0F);
			tessellator.addVertexWithUV(x, y, z, (i + 1) / 5.0D, 0.0D);
		}
		tessellator.draw();

		GL11.glPopMatrix();
		GL11.glDisable(GL11.GL_BLEND);
	}

	private void push(double motion, double[] array)
	{
		for (int i = 4; i > 0; i--)
		{
			array[i] = array[i - 1];
		}
		array[0] = motion;
	}
}