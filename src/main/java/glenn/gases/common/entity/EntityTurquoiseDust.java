package glenn.gases.common.entity;

import glenn.gases.Gases;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EntityTurquoiseDust extends EntityDust
{
	public EntityTurquoiseDust(World world)
	{
		super(world);
	}

	public EntityTurquoiseDust(World world, double x, double y, double z)
	{
		super(world, x, y, z);
	}

	public EntityTurquoiseDust(World world, EntityLivingBase thrower)
	{
		super(world, thrower);
	}

	@Override
	public int getColor()
	{
		return Gases.gasTypeElectric.color | 0xFF;
	}
}