package glenn.gases.common.worldgen;

import glenn.gases.Gases;

import java.util.Random;

import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderFlat;
import net.minecraft.world.gen.feature.WorldGenerator;
import cpw.mods.fml.common.IWorldGenerator;

public class WorldGeneratorGases implements IWorldGenerator
{
	private WorldGenerator gasPipeGen;

	public WorldGeneratorGases()
	{
		gasPipeGen = new WorldGenPipes();
	}

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
	{
		// Don't generate in flat worlds
		if (world.getWorldInfo().getTerrainType() == WorldType.FLAT)
		{
			return;
		}

		int chunk_X = chunkX * 16;
		int chunk_Z = chunkZ * 16;

		if (world.provider.dimensionId == 0)
		{
			for (int i = 0; i < Gases.configurations.worldGeneration.overworld.gasPipe.generationChecks; i++)
			{
				gasPipeGen.generate(world, random, chunk_X + random.nextInt(16), 16 + random.nextInt(32), chunk_Z + random.nextInt(16));
			}
		}
	}
}