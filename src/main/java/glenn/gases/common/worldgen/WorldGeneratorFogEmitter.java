package glenn.gases.common.worldgen;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import glenn.gases.Gases;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;

public class WorldGeneratorFogEmitter implements IWorldGenerator
{
	@Override
	public void generate(Random random, int chunk_X, int chunk_Z, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
	{
		if (world.provider.dimensionId == 0)
		{
			int checks = Gases.configurations.worldGeneration.overworld.crackedBedrock.generationChecks;
			for (int i = 0; i < checks; i++)
			{
				int x = chunk_X * 16 + random.nextInt(16);
				int y = 0 + random.nextInt(4);
				int z = chunk_Z * 16 + random.nextInt(16);
				if (world.getBlock(x, y, z) == Blocks.bedrock)
				{
					world.setBlock(x, y, z, Gases.blocks.whisperingFogEmitter);
				}
			}
		}
	}
}
