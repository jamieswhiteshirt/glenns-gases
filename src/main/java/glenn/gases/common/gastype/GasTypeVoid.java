package glenn.gases.common.gastype;

import java.util.Random;

import net.minecraft.world.World;
import glenn.gasesframework.api.Combustibility;
import glenn.gasesframework.api.gastype.GasType;

public class GasTypeVoid extends GasType
{
	public GasTypeVoid(boolean isIndustrial, int gasID, String name, int color, int opacity, int density, Combustibility combustibility)
	{
		super(isIndustrial, gasID, name, color, opacity, density, combustibility);
	}

	/**
	 * Get the decay of a gas. This triggers every time the gas block ticks.
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @param random
	 * @return
	 */
	@Override
	public int getDissipation(World world, int x, int y, int z, Random random)
	{
		int decay = world.getBlockLightValue(x, y, z) - 9 + opacity;
		return decay > 0 ? decay : super.getDissipation(world, x, y, z, random);
	}
}