package glenn.gases.common.gastype;

import java.util.Random;

import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import glenn.gasesframework.api.Combustibility;
import glenn.gasesframework.api.gastype.GasType;

public class GasTypeBlackDamp extends GasType
{
	public GasTypeBlackDamp(boolean isIndustrial, int gasID, String name, int color, int opacity, int density, Combustibility combustibility)
	{
		super(isIndustrial, gasID, name, color, opacity, density, combustibility);
	}

	/**
	 * Called randomly on the client when the player is around a gas block of
	 * this type.
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @param random
	 */
	@Override
	public void randomDisplayTick(World world, int x, int y, int z, Random random)
	{
		float f2 = 0.0F;
		switch (world.difficultySetting)
		{
			case PEACEFUL:
				f2 = 40.0F;
				break;
			case EASY:
				f2 = 60.0F;
				break;
			case NORMAL:
				f2 = 80.0F;
				break;
			case HARD:
				f2 = 100.0F;
				break;
		}

		if (random.nextInt((int) (f2)) == 0)
		{
			world.spawnParticle("depthsuspend", x + random.nextFloat(), y + random.nextFloat(), z + random.nextFloat(), 0.0D, 0.0D, 0.0D);
		}
	}

	/**
	 * Get the relative Y coordinate of the bottom side of the gas block.
	 * 
	 * @param blockAccess
	 * @param x
	 * @param y
	 * @param z
	 * @param metadata
	 * @return
	 */
	public double getMinY(IBlockAccess blockAccess, int x, int y, int z, int metadata)
	{
		return 0.0D;
	}

	/**
	 * Get the relative Y coordinate of the bottom side of the gas block.
	 * 
	 * @param blockAccess
	 * @param x
	 * @param y
	 * @param z
	 * @param metadata
	 * @return
	 */
	public double getMaxY(IBlockAccess blockAccess, int x, int y, int z, int metadata)
	{
		return 1.0D;
	}
}