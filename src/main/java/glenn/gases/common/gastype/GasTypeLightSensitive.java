package glenn.gases.common.gastype;

import glenn.gases.Gases;
import glenn.gasesframework.api.Combustibility;
import glenn.gasesframework.api.ExtendedGasEffectsBase;
import glenn.gasesframework.api.gastype.GasType;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

/** Gas Type for Iocalfaeus. */
public class GasTypeLightSensitive extends GasType
{
	/** Creates this gas type. */
	public GasTypeLightSensitive(boolean isIndustrial, int gasIndex, String name, int color, int opacity, int density, Combustibility combustibility)
	{
		super(isIndustrial, gasIndex, name, color, opacity, density, combustibility);
	}

	private void warmSurroundingStone(World world, int x, int y, int z, Random random, float brightness)
	{
		if (brightness >= 0.0F)
		{
			brightness *= random.nextFloat() * 2.0F;
			float chance = random.nextFloat();
			int radius = 2;
			for (int i = -radius; i <= radius; i++)
			{
				for (int j = -radius; j <= radius; j++)
				{
					for (int k = -radius; k <= radius; k++)
					{
						int r2 = i * i + j * j + k * k;
						if (r2 > radius * radius + 2)
							continue;
						int x1 = x + i;
						int y1 = y + j;
						int z1 = z + k;
						Block otherBlock = world.getBlock(x1, y1, z1);
						if (otherBlock == Blocks.stone || otherBlock == Gases.blocks.warmStone)
						{
							float r = MathHelper.sqrt_float(r2);
							// int maxMetadata =
							// MathHelper.ceiling_float_int(brightness * 16.0F *
							// (0.5F + radius - r) / (1.0F + radius));
							int otherMetadata = otherBlock == Blocks.stone ? -1 : world.getBlockMetadata(x1, y1, z1);
							if (chance < brightness * (1.0F + radius - r) / (2.0F + radius))
							{
								if (otherMetadata >= 15)
								{
									world.setBlock(x1, y1, z1, Blocks.lava);
								}
								else if (otherMetadata == -1)
								{
									world.setBlock(x1, y1, z1, Gases.blocks.warmStone);
								}
								else
								{
									world.setBlockMetadataWithNotify(x1, y1, z1, otherMetadata + 1, 3);
								}
							}
						}
					}
				}
			}
		}
	}

	private void slowSurroundingEntities(World world, int x, int y, int z, Random random, float r)
	{
		List<EntityLivingBase> nearbyEntities = world.getEntitiesWithinAABB(EntityLivingBase.class, AxisAlignedBB.getBoundingBox(x - r, y - r, z - r, x + 1.0D + r, y + 1.0D + r, z + 1.0D + r));
		for (EntityLivingBase entity : nearbyEntities)
		{
			ExtendedGasEffectsBase.get(entity).increment(ExtendedGasEffectsBase.EffectType.SLOWNESS, 150);
		}
	}

	/**
	 * Called before a gas block of this type ticks.
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @param random
	 */
	public void preTick(World world, int x, int y, int z, Random random)
	{
		if (!world.isRemote)
		{
			float brightness = world.getLightBrightness(x, y, z) - 0.2F;
			warmSurroundingStone(world, x, y, z, random, brightness);
			if (brightness > 0.0f)
			{
				slowSurroundingEntities(world, x, y, z, random, brightness * 10.0f);
			}
		}
	}

	/**
	 * Called at the end of the gas block tick. If this returns true, a new tick
	 * is guaranteed.
	 * 
	 * @param world
	 * @param x
	 * @param y
	 * @param z
	 * @param random
	 * @return
	 */
	public boolean requiresNewTick(World world, int x, int y, int z, Random random)
	{
		return true;
	}
}
