package glenn.gases.common.item;

import glenn.gases.Gases;
import glenn.gasesframework.api.ExtendedGasEffectsBase.EffectType;
import glenn.gasesframework.api.gastype.GasType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;

public class ItemPrimitiveRespirator extends ItemRespirator
{
	public ItemPrimitiveRespirator()
	{
		super(Gases.PRIMITIVE_RESPIRATOR, "gases:textures/models/armor/respirator_primitive.png");
	}

	@Override
	protected boolean prevent(EffectType effect)
	{
		return effect == EffectType.SUFFOCATION;
	}
}