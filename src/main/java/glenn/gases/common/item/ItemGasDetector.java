package glenn.gases.common.item;

import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.gastype.GasType;
import glenn.gasesframework.api.item.ISampler;
import glenn.moddingutils.DVec;
import glenn.moddingutils.DVec2;
import glenn.moddingutils.IVec;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemGasDetector extends Item implements ISampler
{
	public IIcon overlayIcon;
	public IIcon emptyOverlayIcon;

	public ItemGasDetector()
	{
		super();
		this.setHasSubtypes(true);
	}

	public GasType getGasType(ItemStack itemstack)
	{
		return GFAPI.registry.getGasTypeByID(itemstack.getItemDamage());
	}

	@Override
	public String getItemStackDisplayName(ItemStack itemstack)
	{
		if (itemstack.getItemDamage() > 0)
		{
			String s = StatCollector.translateToLocal(getGasType(itemstack).getUnlocalizedName() + ".name");
			return StatCollector.translateToLocalFormatted(getUnlocalizedNameInefficiently(itemstack) + ".name.filled", s);
		}
		return super.getItemStackDisplayName(itemstack);
	}

	/**
	 * Called whenever this item is equipped and the right mouse button is
	 * pressed. Args: itemStack, world, entityPlayer
	 */
	@Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer entityPlayer)
	{
		if (itemstack.getItemDamage() != 0)
		{
			for (int i = 0; i < 100; i++)
			{
				double x = Math.sin(entityPlayer.rotationYaw * Math.PI / -180.0D);
				double y = Math.sin(entityPlayer.rotationPitch * Math.PI / -180.0D);
				double y2 = Math.cos(entityPlayer.rotationPitch * Math.PI / -180.0D);
				double z = Math.cos(entityPlayer.rotationYaw * Math.PI / -180.0D);
				DVec vec = new DVec(x * y2, y, z * y2).scale(Math.sqrt(i));

				DVec2 vec2 = DVec2.randomNormalizedVec(world.rand).scale(Math.sqrt(world.rand.nextDouble() * i) * 0.5D);
				DVec vec3 = new DVec(vec2.x, vec2.y, 0.0D);
				vec3.xRotate(entityPlayer.rotationPitch * Math.PI / -180.0D);
				vec3.yRotate(entityPlayer.rotationYaw * Math.PI / -180.0D);

				vec.add(entityPlayer.posX, entityPlayer.posY + entityPlayer.getEyeHeight(), entityPlayer.posZ).add(vec3);

				IVec ivec = new IVec((int) Math.round(vec.x), (int) Math.round(vec.y), (int) Math.round(vec.z));

				if (getGasType(itemstack) == GFAPI.implementation.getGasType(world, ivec.x, ivec.y, ivec.z))
				{
					world.playSoundAtEntity(entityPlayer, "liquid.lavapop", 2.0F, 0.5F + (100.0F - i) * 0.015F);
					break;
				}
			}
		}

		return itemstack;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean requiresMultipleRenderPasses()
	{
		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public int getColorFromItemStack(ItemStack par1ItemStack, int par2)
	{
		return par2 > 0 ? 16777215 : this.getColorFromDamage(par1ItemStack.getItemDamage());
	}

	@SideOnly(Side.CLIENT)
	public int getColorFromDamage(int par1)
	{
		if (par1 > 0)
		{
			return GFAPI.registry.getGasTypeByID(par1).color >> 8;
		}
		else
		{
			return 16777215;
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister par1IconRegister)
	{
		itemIcon = par1IconRegister.registerIcon(this.getIconString());
		overlayIcon = par1IconRegister.registerIcon(this.getIconString() + "_overlay");
		emptyOverlayIcon = par1IconRegister.registerIcon(this.getIconString() + "_overlay_empty");
	}

	@SideOnly(Side.CLIENT)
	/**
	 * Gets an icon index based on an item's damage value and the given render
	 * pass
	 */
	@Override
	public IIcon getIconFromDamageForRenderPass(int par1, int par2)
	{
		return par2 == 0 ? (par1 > 0 ? overlayIcon : emptyOverlayIcon) : itemIcon;
	}

	@SideOnly(Side.CLIENT)
	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public IIcon getIconFromDamage(int par1)
	{
		return this.itemIcon;
	}

	@Override
	public ItemStack setGasType(ItemStack itemstack, GasType gasType)
	{
		if (gasType != null)
		{
			itemstack.setItemDamage(gasType.gasID);
		}
		else
		{
			itemstack.setItemDamage(0);
		}
		return itemstack;
	}
}