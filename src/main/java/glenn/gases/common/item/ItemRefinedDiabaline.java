package glenn.gases.common.item;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemRefinedDiabaline extends Item
{
	public boolean glowState = false;
	private IIcon glowingIcon;

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister par1IconRegister)
	{
		itemIcon = par1IconRegister.registerIcon(this.getIconString());
		glowingIcon = par1IconRegister.registerIcon(this.getIconString() + "_glowing");
	}

	@Override
	public float getSmeltingExperience(ItemStack stack)
	{
		return 2;
	}

	@SideOnly(Side.CLIENT)
	/**
	 * Gets an icon index based on an item's damage value
	 */
	@Override
	public IIcon getIconFromDamage(int par1)
	{
		return glowState ? glowingIcon : this.itemIcon;
	}

	@Override
	public String getItemStackDisplayName(ItemStack par1ItemStack)
	{
		if (glowState)
		{
			return StatCollector.translateToLocal(getUnlocalizedNameInefficiently(par1ItemStack) + ".name.glowing");
		}
		else
		{
			return StatCollector.translateToLocal(getUnlocalizedNameInefficiently(par1ItemStack) + ".name");
		}
	}
}
