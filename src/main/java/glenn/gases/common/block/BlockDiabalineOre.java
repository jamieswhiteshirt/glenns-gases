package glenn.gases.common.block;

import glenn.gases.Gases;
import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.gastype.GasType;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockOre;
import net.minecraft.item.Item;
import net.minecraft.world.World;

/**
 * Diabaline ore block. Must be refined into refined diabaline in a gas furnace.
 */
public class BlockDiabalineOre extends BlockOre
{
	/** Current glowing status. Determines texture. */
	protected boolean glowing;

	/**
	 * Creates a new Diabaline Ore block.
	 * 
	 * @param glowing
	 *            Sets the block to glowing if true or non-glowing if false.
	 */
	public BlockDiabalineOre(boolean glowing)
	{
		super();
		this.glowing = glowing;

		if (glowing)
			setLightLevel(4.0F / 16.0F);
		setHardness(1.5F);
		setResistance(10.0F);
		setStepSound(Block.soundTypePiston);
	}

	/**
	 * Returns dropped item.
	 * 
	 * @param metadata
	 *            Metadata of the block. Disregarded here.
	 * @param random
	 *            Random object passed from Forge to prevent excessive
	 *            allocation of Random objects. Disregarded here.
	 * @param fortune
	 *            Level of fortune the tool is used on. Disregarded here.
	 */
	@Override
	public Item getItemDropped(int metadata, Random random, int fortune)
	{
		return Item.getItemFromBlock(Gases.blocks.diabalineOre);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block neighborBlock)
	{
		onBlockAdded(world, x, y, z);
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z)
	{
		boolean isSurroundedByGas = false;

		for (int side = 0; side < 6; side++)
		{
			int xDirection = x + (side == 4 ? 1 : (side == 5 ? -1 : 0));
			int yDirection = y + (side == 0 ? 1 : (side == 1 ? -1 : 0));
			int zDirection = z + (side == 2 ? 1 : (side == 3 ? -1 : 0));

			GasType type = GFAPI.implementation.getGasType(world, xDirection, yDirection, zDirection);
			if (type != null && type.isIndustrial && type != GFAPI.gasTypeAir)
			{
				isSurroundedByGas = true;
				break;
			}
		}

		if (isSurroundedByGas & !glowing)
		{
			world.setBlock(x, y, z, Gases.blocks.diabalineOreGlowing);
		}
		else if (!isSurroundedByGas & glowing)
		{
			world.setBlock(x, y, z, Gases.blocks.diabalineOre);
		}
	}

	@Override
	public Item getItem(World par1World, int par2, int par3, int par4)
	{
		return Item.getItemFromBlock(Gases.blocks.diabalineOre);
	}
}