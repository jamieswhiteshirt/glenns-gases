package glenn.gases.common.reaction;

import glenn.gasesframework.api.reaction.BlockReaction;
import glenn.gasesframework.api.reaction.environment.IBlockReactionEnvironment;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;

public class ReactionExtinguish extends BlockReaction
{
	@Override
	public void react(IBlockReactionEnvironment environment)
	{
		Block b = environment.getB();
		if (b == Blocks.torch || b == Blocks.fire)
		{
			environment.breakB();
		}
	}
}