package glenn.gases.common.reaction;

import glenn.gases.Gases;
import glenn.gases.common.entity.EntityBlueDust;
import glenn.gases.common.entity.EntityDust;
import net.minecraft.item.ItemStack;

public class EntityReactionTurquoiseDustDrop extends EntityReactionDustDrop
{
	@Override
	public ItemStack drop(EntityDust entity)
	{
		if (entity instanceof EntityBlueDust)
		{
			return new ItemStack(Gases.items.turquoiseDust);
		}
		else
		{
			return null;
		}
	}
}
