package glenn.gases.common.reaction;

import glenn.gases.common.entity.EntityDust;
import glenn.gasesframework.api.GFAPI;
import glenn.gasesframework.api.PartialGasStack;
import glenn.gasesframework.api.reaction.EntityReaction;
import glenn.gasesframework.api.reaction.environment.IEntityReactionEnvironment;
import net.minecraft.item.ItemStack;

public abstract class EntityReactionDustDrop extends EntityReaction
{
	@Override
	public void react(IEntityReactionEnvironment environment)
	{
		if (environment.getB() instanceof EntityDust)
		{
			ItemStack droppedItem = drop((EntityDust) environment.getB());
			if (droppedItem != null)
			{
				PartialGasStack a = environment.getA();
				if (environment.getRandom().nextInt(8) < a.partialAmount)
				{
					environment.dropItem(droppedItem);
				}
				environment.setA(new PartialGasStack(GFAPI.gasTypeAir));
			}
		}
	}

	public abstract ItemStack drop(EntityDust entity);
}
