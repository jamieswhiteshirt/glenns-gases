package glenn.gases;

import glenn.gases.api.IGGRegistry;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

import java.util.*;

public class Registry implements IGGRegistry
{
	private final Set<Block> coalDustEmissionBlocks = Collections.newSetFromMap(new IdentityHashMap<Block, Boolean>());

	@Override
	public void registerCoalDustEmissionBlock(Block block)
	{
		coalDustEmissionBlocks.add(block);
	}

	@Override
	public boolean isCoalDustEmissionBlock(Block block)
	{
		return coalDustEmissionBlocks.contains(block);
	}

	private final Map<Block, Block> rustedBlocks = new IdentityHashMap<Block, Block>();
	private final Map<Block, Block> unrustedBlocks = new IdentityHashMap<Block, Block>();
	private final Map<Item, Item> rustedItems = new IdentityHashMap<Item, Item>();
	private final Map<Item, Item> unrustedItems = new IdentityHashMap<Item, Item>();
	private final Set<String> rustableMaterials = new HashSet<String>();

	@Override
	public void registerRustedBlock(Block block, Block rustedBlock)
	{
		rustedBlocks.put(block, rustedBlock);
		unrustedBlocks.put(rustedBlock, block);
		rustedItems.put(Item.getItemFromBlock(block), Item.getItemFromBlock(rustedBlock));
	}

	@Override
	public Block getRustedBlock(Block block)
	{
		return rustedBlocks.get(block);
	}

	@Override
	public Block getUnrustedBlock(Block rustedBlock)
	{
		return unrustedBlocks.get(rustedBlock);
	}

	@Override
	public void registerRustedItem(Item item, Item rustedItem)
	{
		rustedItems.put(item, rustedItem);
		unrustedItems.put(rustedItem, item);
	}

	@Override
	public Item getRustedItem(Item item)
	{
		Item rustedItem = rustedItems.get(item);
		if (rustedItem == null)
		{
			Block block = Block.getBlockFromItem(item);
			if (block != Blocks.air)
			{
				Block rustedBlock = getRustedBlock(block);
				if (rustedBlock != null)
				{
					return Item.getItemFromBlock(rustedBlock);
				}
			}
		}
		return rustedItem;
	}

	@Override
	public Item getUnrustedItem(Item rustedItem)
	{
		Item unrustedItem = unrustedItems.get(rustedItem);
		if (unrustedItem == null)
		{
			Block rustedBlock = Block.getBlockFromItem(rustedItem);
			if (rustedBlock != Blocks.air)
			{
				Block unrustedBlock = getUnrustedBlock(rustedBlock);
				if (unrustedBlock != null)
				{
					return Item.getItemFromBlock(unrustedBlock);
				}
			}
		}
		return unrustedItem;
	}

	@Override
	public void registerRustableMaterial(String material)
	{
		rustableMaterials.add(material);
	}

	@Override
	public boolean isRustableMaterial(String material)
	{
		return rustableMaterials.contains(material);
	}
}
